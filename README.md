Role Name
=========

Install VScode

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: desktop
      roles:
         - dietrichliko.vscode

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at